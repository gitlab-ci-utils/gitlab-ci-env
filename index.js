'use strict';

/**
 * Returns an object containing all of the predefined environment variables
 * provided by GitLab CI.
 *
 * @module gitlab-ci-env
 * @returns {object} Object containing all of the predefined environment
 *                   variables provided by GitLab CI.
 * @static
 */
module.exports = Object.freeze({
    chat: {
        channel: process.env.CHAT_CHANNEL,
        input: process.env.CHAT_INPUT,
        userID: process.env.CHAT_USER_ID
    },
    ci: {
        apiGraphqlUrl: process.env.CI_API_GRAPHQL_URL,
        apiV4Url: process.env.CI_API_V4_URL,
        buildsDir: process.env.CI_BUILDS_DIR,
        commit: {
            author: process.env.CI_COMMIT_AUTHOR,
            beforeSha: process.env.CI_COMMIT_BEFORE_SHA,
            branch: process.env.CI_COMMIT_BRANCH,
            description: process.env.CI_COMMIT_DESCRIPTION,
            message: process.env.CI_COMMIT_MESSAGE,
            ref: {
                name: process.env.CI_COMMIT_REF_NAME,
                slug: process.env.CI_COMMIT_REF_SLUG
            },
            refProtected: process.env.CI_COMMIT_REF_PROTECTED,
            sha: process.env.CI_COMMIT_SHA,
            shortSha: process.env.CI_COMMIT_SHORT_SHA,
            tag: process.env.CI_COMMIT_TAG,
            tagMessage: process.env.CI_COMMIT_TAG_MESSAGE,
            timestamp: process.env.CI_COMMIT_TIMESTAMP,
            title: process.env.CI_COMMIT_TITLE
        },
        concurrentID: process.env.CI_CONCURRENT_ID,
        concurrentProjectID: process.env.CI_CONCURRENT_PROJECT_ID,
        configPath: process.env.CI_CONFIG_PATH,
        debug: {
            services: process.env.CI_DEBUG_SERVICES,
            trace: process.env.CI_DEBUG_TRACE
        },
        defaultBranch: process.env.CI_DEFAULT_BRANCH,
        dependencyProxy: {
            directGroupImagePrefix:
                process.env.CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX,
            groupImagePrefix:
                process.env.CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX,
            password: process.env.CI_DEPENDENCY_PROXY_PASSWORD,
            server: process.env.CI_DEPENDENCY_PROXY_SERVER,
            user: process.env.CI_DEPENDENCY_PROXY_USER
        },
        deploy: {
            freeze: process.env.CI_DEPLOY_FREEZE,
            password: process.env.CI_DEPLOY_PASSWORD,
            user: process.env.CI_DEPLOY_USER
        },
        environment: {
            action: process.env.CI_ENVIRONMENT_ACTION,
            isDisposable: process.env.CI_DISPOSABLE_ENVIRONMENT,
            name: process.env.CI_ENVIRONMENT_NAME,
            slug: process.env.CI_ENVIRONMENT_SLUG,
            tier: process.env.CI_ENVIRONMENT_TIER,
            url: process.env.CI_ENVIRONMENT_URL
        },
        externalPullRequest: {
            iid: process.env.CI_EXTERNAL_PULL_REQUEST_IID,
            source: {
                branch: {
                    name: process.env
                        .CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME,
                    sha: process.env.CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_SHA
                },
                repository:
                    process.env.CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY
            },
            target: {
                branch: {
                    name: process.env
                        .CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_NAME,
                    sha: process.env.CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_SHA
                },
                repository:
                    process.env.CI_EXTERNAL_PULL_REQUEST_TARGET_REPOSITORY
            }
        },
        hasOpenRequirements: process.env.CI_HAS_OPEN_REQUIREMENTS,
        isCI: process.env.CI,
        job: {
            groupName: process.env.CI_JOB_GROUP_NAME,
            id: process.env.CI_JOB_ID,
            image: process.env.CI_JOB_IMAGE,
            manual: process.env.CI_JOB_MANUAL,
            name: process.env.CI_JOB_NAME,
            nameSlug: process.env.CI_JOB_NAME_SLUG,
            stage: process.env.CI_JOB_STAGE,
            startedAt: process.env.CI_JOB_STARTED_AT,
            status: process.env.CI_JOB_STATUS,
            timeout: process.env.CI_JOB_TIMEOUT,
            token: process.env.CI_JOB_TOKEN,
            url: process.env.CI_JOB_URL
        },
        kubeConfig: process.env.KUBECONFIG,
        kubernetesActive: process.env.CI_KUBERNETES_ACTIVE,
        mergeRequest: {
            approved: process.env.CI_MERGE_REQUEST_APPROVED,
            assignees: process.env.CI_MERGE_REQUEST_ASSIGNEES,
            description: process.env.CI_MERGE_REQUEST_DESCRIPTION,
            descriptionTruncated:
                process.env.CI_MERGE_REQUEST_DESCRIPTION_IS_TRUNCATED,
            diff: {
                baseSha: process.env.CI_MERGE_REQUEST_DIFF_BASE_SHA,
                id: process.env.CI_MERGE_REQUEST_DIFF_ID
            },
            draft: process.env.CI_MERGE_REQUEST_DRAFT,
            eventType: process.env.CI_MERGE_REQUEST_EVENT_TYPE,
            id: process.env.CI_MERGE_REQUEST_ID,
            iid: process.env.CI_MERGE_REQUEST_IID,
            labels: process.env.CI_MERGE_REQUEST_LABELS,
            milestones: process.env.CI_MERGE_REQUEST_MILESTONE,
            project: {
                id: process.env.CI_MERGE_REQUEST_PROJECT_ID,
                path: process.env.CI_MERGE_REQUEST_PROJECT_PATH,
                url: process.env.CI_MERGE_REQUEST_PROJECT_URL
            },
            refPath: process.env.CI_MERGE_REQUEST_REF_PATH,
            source: {
                branch: {
                    name: process.env.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME,
                    protected:
                        process.env.CI_MERGE_REQUEST_SOURCE_BRANCH_PROTECTED,
                    sha: process.env.CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
                },
                project: {
                    id: process.env.CI_MERGE_REQUEST_SOURCE_PROJECT_ID,
                    path: process.env.CI_MERGE_REQUEST_SOURCE_PROJECT_PATH,
                    url: process.env.CI_MERGE_REQUEST_SOURCE_PROJECT_URL
                }
            },
            squashOnMerge: process.env.CI_MERGE_REQUEST_SQUASH_ON_MERGE,
            target: {
                branch: {
                    name: process.env.CI_MERGE_REQUEST_TARGET_BRANCH_NAME,
                    protected:
                        process.env.CI_MERGE_REQUEST_TARGET_BRANCH_PROTECTED,
                    sha: process.env.CI_MERGE_REQUEST_TARGET_BRANCH_SHA
                }
            },
            title: process.env.CI_MERGE_REQUEST_TITLE
        },
        node: {
            index: process.env.CI_NODE_INDEX,
            total: process.env.CI_NODE_TOTAL
        },
        pages: {
            domain: process.env.CI_PAGES_DOMAIN,
            hostname: process.env.CI_PAGES_HOSTNAME,
            url: process.env.CI_PAGES_URL
        },
        pipeline: {
            createdAt: process.env.CI_PIPELINE_CREATED_AT,
            id: process.env.CI_PIPELINE_ID,
            iid: process.env.CI_PIPELINE_IID,
            name: process.env.CI_PIPELINE_NAME,
            schedule: {
                description: process.env.CI_PIPELINE_SCHEDULE_DESCRIPTION
            },
            source: process.env.CI_PIPELINE_SOURCE,
            triggered: process.env.CI_PIPELINE_TRIGGERED,
            url: process.env.CI_PIPELINE_URL
        },
        project: {
            classificationLabel: process.env.CI_PROJECT_CLASSIFICATION_LABEL,
            description: process.env.CI_PROJECT_DESCRIPTION,
            dir: process.env.CI_PROJECT_DIR,
            id: process.env.CI_PROJECT_ID,
            name: process.env.CI_PROJECT_NAME,
            namespace: {
                fullPath: process.env.CI_PROJECT_NAMESPACE,
                id: process.env.CI_PROJECT_NAMESPACE_ID,
                slug: process.env.CI_PROJECT_NAMESPACE_SLUG
            },
            openMergeRequests: process.env.CI_OPEN_MERGE_REQUESTS,
            path: process.env.CI_PROJECT_PATH,
            pathSlug: process.env.CI_PROJECT_PATH_SLUG,
            repositoryLanguages: process.env.CI_PROJECT_REPOSITORY_LANGUAGES,
            rootNamespace: process.env.CI_PROJECT_ROOT_NAMESPACE,
            title: process.env.CI_PROJECT_TITLE,
            url: process.env.CI_PROJECT_URL,
            visibility: process.env.CI_PROJECT_VISIBILITY
        },
        registry: {
            image: process.env.CI_REGISTRY_IMAGE,
            password: process.env.CI_REGISTRY_PASSWORD,
            registry: process.env.CI_REGISTRY,
            url: process.env.CI_REPOSITORY_URL,
            user: process.env.CI_REGISTRY_USER
        },
        release: {
            description: process.env.CI_RELEASE_DESCRIPTION
        },
        runner: {
            description: process.env.CI_RUNNER_DESCRIPTION,
            executableArch: process.env.CI_RUNNER_EXECUTABLE_ARCH,
            id: process.env.CI_RUNNER_ID,
            revision: process.env.CI_RUNNER_REVISION,
            shortToken: process.env.CI_RUNNER_SHORT_TOKEN,
            tags: process.env.CI_RUNNER_TAGS,
            version: process.env.CI_RUNNER_VERSION
        },
        server: {
            fqdn: process.env.CI_SERVER_FQDN,
            host: process.env.CI_SERVER_HOST,
            isServer: process.env.CI_SERVER,
            name: process.env.CI_SERVER_NAME,
            port: process.env.CI_SERVER_PORT,
            protocol: process.env.CI_SERVER_PROTOCOL,
            revision: process.env.CI_SERVER_REVISION,
            ssh: {
                host: process.env.CI_SERVER_SHELL_SSH_HOST,
                port: process.env.CI_SERVER_SHELL_SSH_PORT
            },
            tlsCaFile: process.env.CI_SERVER_TLS_CA_FILE,
            tlsCertFile: process.env.CI_SERVER_TLS_CERT_FILE,
            tlsKeyFile: process.env.CI_SERVER_TLS_KEY_FILE,
            url: process.env.CI_SERVER_URL,
            version: {
                major: process.env.CI_SERVER_VERSION_MAJOR,
                minor: process.env.CI_SERVER_VERSION_MINOR,
                patch: process.env.CI_SERVER_VERSION_PATCH,
                version: process.env.CI_SERVER_VERSION
            }
        },
        sharedEnvironment: process.env.CI_SHARED_ENVIRONMENT,
        templateRegistryHost: process.env.CI_TEMPLATE_REGISTRY_HOST,
        trigger: {
            payload: process.env.TRIGGER_PAYLOAD,
            shortToken: process.env.CI_TRIGGER_SHORT_TOKEN
        }
    },
    gitlab: {
        features: process.env.GITLAB_FEATURES,
        fipsMode: process.env.CI_GITLAB_FIPS_MODE,
        isCI: process.env.GITLAB_CI,
        user: {
            email: process.env.GITLAB_USER_EMAIL,
            id: process.env.GITLAB_USER_ID,
            login: process.env.GITLAB_USER_LOGIN,
            name: process.env.GITLAB_USER_NAME
        }
    }
});
