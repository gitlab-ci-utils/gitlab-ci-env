# GitLab CI Env

GitLab CI Env returns an object containing all of the predefined environment variables provided by GitLab CI.

## Usage

```js
const gitlabEnv = require('gitlab-ci-env');

const projectName = gitlabEnv.ci.project.name;
const commitSha = gitlabEnv.ci.commit.sha;
```

## Variable reference

The GitLab predefined environment variables are grouped by function. Generally this hierarchy can be derived from the name, for example `CI_COMMIT_REF_NAME` and `CI_COMMIT_REF_SLUG` are provided via `ci.commit.ref.name` and `ci.commit.ref.slug` respectively. There are some exceptions, which are included as properties in the most appropriate functional location.

A complete mapping of the GitLab predefined environment variables to the object provided by this module is given below for reference (values equal variable names).

```js
{
  chat: {
    channel: 'CHAT_CHANNEL',
    input: 'CHAT_INPUT',
    userID: 'CHAT_USER_ID'
  },
  ci: {
    apiGraphqlUrl: 'CI_API_GRAPHQL_URL',
    apiV4Url: 'CI_API_V4_URL',
    buildsDir: 'CI_BUILDS_DIR',
    commit: {
      author: 'CI_COMMIT_AUTHOR',
      beforeSha: 'CI_COMMIT_BEFORE_SHA',
      branch: 'CI_COMMIT_BRANCH',
      description: 'CI_COMMIT_DESCRIPTION',
      message: 'CI_COMMIT_MESSAGE',
      ref: {
        name: 'CI_COMMIT_REF_NAME',
        slug: 'CI_COMMIT_REF_SLUG',
      },
      refProtected: 'CI_COMMIT_REF_PROTECTED',
      sha: 'CI_COMMIT_SHA',
      shortSha: 'CI_COMMIT_SHORT_SHA',
      tag: 'CI_COMMIT_TAG',
      tagMessage: 'CI_COMMIT_TAG_MESSAGE',
      timestamp: 'CI_COMMIT_TIMESTAMP',
      title: 'CI_COMMIT_TITLE',
    },
    concurrentID: 'CI_CONCURRENT_ID',
    concurrentProjectID: 'CI_CONCURRENT_PROJECT_ID',
    configPath: 'CI_CONFIG_PATH',
    debug: {
        services: 'CI_DEBUG_SERVICES',
        trace: 'CI_DEBUG_TRACE'
    },
    defaultBranch: 'CI_DEFAULT_BRANCH',
    dependencyProxy: {
        directGroupImagePrefix: 'CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX',
        groupImagePrefix: 'CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX',
        password: 'CI_DEPENDENCY_PROXY_PASSWORD',
        server: 'CI_DEPENDENCY_PROXY_SERVER',
        user: 'CI_DEPENDENCY_PROXY_USER'
    },
    deploy: {
      freeze: 'CI_DEPLOY_FREEZE',
      password: 'CI_DEPLOY_PASSWORD',
      user: 'CI_DEPLOY_USER',
    },
    environment: {
      action: 'CI_ENVIRONMENT_ACTION',
      isDisposable: 'CI_DISPOSABLE_ENVIRONMENT',
      name: 'CI_ENVIRONMENT_NAME',
      slug: 'CI_ENVIRONMENT_SLUG',
      tier: 'CI_ENVIRONMENT_TIER',
      url: 'CI_ENVIRONMENT_URL',
    },
    externalPullRequest: {
      iid: 'CI_EXTERNAL_PULL_REQUEST_IID',
      source: {
        branch: {
          name: 'CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME',
          sha: 'CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_SHA'
        },
        repository: 'CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY'
      },
      target: {
        branch: {
          name: 'CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_NAME',
          sha: 'CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_SHA'
        },
        repository: 'CI_EXTERNAL_PULL_REQUEST_TARGET_REPOSITORY'
      }
    },
    hasOpenRequirements: 'CI_HAS_OPEN_REQUIREMENTS',
    isCI: 'CI',
    job: {
      groupName: 'CI_JOB_GROUP_NAME',
      id: 'CI_JOB_ID',
      image: 'CI_JOB_IMAGE',
      manual: 'CI_JOB_MANUAL',
      name: 'CI_JOB_NAME',
      nameSlug: 'CI_JOB_NAME_SLUG',
      stage: 'CI_JOB_STAGE',
      startedAt: 'CI_JOB_STARTED_AT',
      status: 'CI_JOB_STATUS',
      timeout: 'CI_JOB_TIMEOUT',
      token: 'CI_JOB_TOKEN',
      url: 'CI_JOB_URL',
    },
    kubeConfig: 'KUBECONFIG',
    kubernetesActive: 'CI_KUBERNETES_ACTIVE',
    mergeRequest: {
      approved: 'CI_MERGE_REQUEST_APPROVED',
      assignees: 'CI_MERGE_REQUEST_ASSIGNEES',
      description: 'CI_MERGE_REQUEST_DESCRIPTION',
      descriptionTruncated: 'CI_MERGE_REQUEST_DESCRIPTION_IS_TRUNCATED',
      diff: {
          baseSha: 'CI_MERGE_REQUEST_DIFF_BASE_SHA',
          id: 'CI_MERGE_REQUEST_DIFF_ID'
      },
      draft: 'CI_MERGE_REQUEST_DRAFT',
      eventType: 'CI_MERGE_REQUEST_EVENT_TYPE',
      id: 'CI_MERGE_REQUEST_ID',
      iid: 'CI_MERGE_REQUEST_IID',
      labels: 'CI_MERGE_REQUEST_LABELS',
      milestones: 'CI_MERGE_REQUEST_MILESTONE',
      project: {
        id: 'CI_MERGE_REQUEST_PROJECT_ID',
        path: 'CI_MERGE_REQUEST_PROJECT_PATH',
        url: 'CI_MERGE_REQUEST_PROJECT_URL',
      },
      refPath: 'CI_MERGE_REQUEST_REF_PATH',
      source: {
        branch: {
          name: 'CI_MERGE_REQUEST_SOURCE_BRANCH_NAME',
          protected: 'CI_MERGE_REQUEST_SOURCE_BRANCH_PROTECTED',
          sha: 'CI_MERGE_REQUEST_SOURCE_BRANCH_SHA',
        },
        project: {
          id: 'CI_MERGE_REQUEST_SOURCE_PROJECT_ID'
          path: 'CI_MERGE_REQUEST_SOURCE_PROJECT_PATH',
          url: 'CI_MERGE_REQUEST_SOURCE_PROJECT_URL'
        },
      },
      squashOnMerge: 'CI_MERGE_REQUEST_SQUASH_ON_MERGE',
      target: {
        branch: {
          name: 'CI_MERGE_REQUEST_TARGET_BRANCH_NAME',
          protected: 'CI_MERGE_REQUEST_TARGET_BRANCH_PROTECTED',
          sha: 'CI_MERGE_REQUEST_TARGET_BRANCH_SHA'
        },
      },
      title: 'CI_MERGE_REQUEST_TITLE',
    },
    node: {
      index: 'CI_NODE_INDEX',
      total: 'CI_NODE_TOTAL',
    },
    pages: {
      domain: 'CI_PAGES_DOMAIN',
      hostname: 'CI_PAGES_HOSTNAME',
      url: 'CI_PAGES_URL',
    },
    pipeline: {
      createdAt: 'CI_PIPELINE_CREATED_AT',
      id: 'CI_PIPELINE_ID',
      iid: 'CI_PIPELINE_IID',
      name: 'CI_PIPELINE_NAME',
      schedule: {
          description: 'CI_PIPELINE_SCHEDULE_DESCRIPTION'
      },
      source: 'CI_PIPELINE_SOURCE',
      triggered: 'CI_PIPELINE_TRIGGERED',
      url: 'CI_PIPELINE_URL',
    },
    project: {
      classificationLabel: 'CI_PROJECT_CLASSIFICATION_LABEL',
      description: 'CI_PROJECT_DESCRIPTION',
      dir: 'CI_PROJECT_DIR',
      id: 'CI_PROJECT_ID',
      name: 'CI_PROJECT_NAME',
      namespace: {
          fullPath: 'CI_PROJECT_NAMESPACE',
          id: 'CI_PROJECT_NAMESPACE_ID',
          slug: 'CI_PROJECT_NAMESPACE_SLUG'
      },
      openMergeRequests: 'CI_OPEN_MERGE_REQUESTS',
      path: 'CI_PROJECT_PATH',
      pathSlug: 'CI_PROJECT_PATH_SLUG',
      repositoryLanguages: 'CI_PROJECT_REPOSITORY_LANGUAGES',
      rootNamespace: 'CI_PROJECT_ROOT_NAMESPACE',
      title: 'CI_PROJECT_TITLE',
      url: 'CI_PROJECT_URL',
      visibility: 'CI_PROJECT_VISIBILITY',
    },
    registry: {
      image: 'CI_REGISTRY_IMAGE',
      password: 'CI_REGISTRY_PASSWORD',
      registry: 'CI_REGISTRY',
      url: 'CI_REPOSITORY_URL',
      user: 'CI_REGISTRY_USER',
    },
    release: {
        description: 'CI_RELEASE_DESCRIPTION'
    },
    runner: {
      description: 'CI_RUNNER_DESCRIPTION',
      executableArch: 'CI_RUNNER_EXECUTABLE_ARCH',
      id: 'CI_RUNNER_ID',
      revision: 'CI_RUNNER_REVISION',
      shortToken: 'CI_RUNNER_SHORT_TOKEN',
      tags: 'CI_RUNNER_TAGS',
      version: 'CI_RUNNER_VERSION',
    },
    server: {
      fqdn: 'CI_SERVER_FQDN',
      host: 'CI_SERVER_HOST',
      isServer: 'CI_SERVER',
      name: 'CI_SERVER_NAME',
      port: 'CI_SERVER_PORT',
      protocol: 'CI_SERVER_PROTOCOL',
      revision: 'CI_SERVER_REVISION',
      ssh: {
        host: `CI_SERVER_SHELL_SSH_HOST`,
        port: `CI_SERVER_SHELL_SSH_PORT`
      },
      tlsCaFile: 'CI_SERVER_TLS_CA_FILE',
      tlsCertFile: 'CI_SERVER_TLS_CERT_FILE',
      tlsKeyFile: 'CI_SERVER_TLS_KEY_FILE',
      url: 'CI_SERVER_URL',
      version: {
        major: 'CI_SERVER_VERSION_MAJOR',
        minor: 'CI_SERVER_VERSION_MINOR',
        patch: 'CI_SERVER_VERSION_PATCH',
        version: 'CI_SERVER_VERSION',
      },
    },
    sharedEnvironment: 'CI_SHARED_ENVIRONMENT',
    templateRegistryHost: `CI_TEMPLATE_REGISTRY_HOST`,
    trigger: {
        payload: `TRIGGER_PAYLOAD`,
        shortToken: `CI_TRIGGER_SHORT_TOKEN`
    }
  },
  gitlab: {
    features: 'GITLAB_FEATURES',
    fipsMode: 'CI_GITLAB_FIPS_MODE',
    isCI: 'GITLAB_CI',
    user: {
      email: 'GITLAB_USER_EMAIL',
      id: 'GITLAB_USER_ID',
      login: 'GITLAB_USER_LOGIN',
      name: 'GITLAB_USER_NAME',
    },
  },
}
```

A complete listing of the GitLab predefined environment variables with descriptions can be found at https://docs.gitlab.com/ee/ci/variables/predefined_variables.html.
