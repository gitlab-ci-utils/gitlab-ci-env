# Changelog

## Unreleased

### Changed

- Added new variables `CI_JOB_GROUP_NAME`, `CI_MERGE_REQUEST_DRAFT`, and
  `CI_PROJECT_NAMESPACE_SLUG` in GitLab 17.10. (#97, #98)

## v10.2.0 (2025-01-16)

### Changed

- Added new variable `CI_PIPELINE_SCHEDULE_DESCRIPTION` in GitLab 17.8. (#96)

## v10.1.0 (2024-12-19)

### Changed

- Added new variable `CI_PAGES_HOSTNAME` in GitLab 17.7. (#95)

### Fixed

- Updated tests for format changes in the
  [GitLab predefined variables tables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html),
  which are used to check for new or removed variables. (#94)

### Miscellaneous

- Updated Renovate config to use use [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  v1.1.0.
- Update to eslint v9 with flat config.

## v10.0.0 (2024-05-16)

### Changed

- BREAKING: Removed properties `ci.job.jwt`, `ci.job.jwtV1`, and
  `ci.job.jwtV2`. The associated variables were previously deprecated and
  will be removed in GitLab 17.0.
- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#93)
- BREAKING: Moved variable `TRIGGER_PAYLOAD` from `ci.pipeline.triggerPayload`
  to `ci.trigger.payload`.
- Added new variable `CI_TRIGGER_SHORT_TOKEN` in GitLab 17.0. (#91)

### Fixed

- Updated tests to exclude `deprecated` predefined variables.

## v9.3.0 (2024-03-21)

### Changed

- Added new variable `CI_SERVER_FQDN` in GitLab 16.10. (#88, #90)

### Fixed

- Fixed GitLab variable tests to accommodate new `Defined for` column in table.
  (#89)

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#87)

## v9.2.0 (2024-01-18)

### Changed

- Added new variable `CI_MERGE_REQUEST_DESCRIPTION_IS_TRUNCATED` in GitLab
  16.8. (#86)

## v9.1.0 (2023-12-21)

### Changed

- Added new variable `CI_MERGE_REQUEST_DESCRIPTION` in GitLab 16.7. (#84)

## v9.0.0 (2023-09-22)

### Changed

- BREAKING: Deprecated support for Node 16 (end-of-life 2023-09-11). Compatible
  with all current and LTS Node releases (`^18.12.0 || >=20.0.0`). (#81)
- Added new variables `CI_MERGE_REQUEST_SQUASH_ON_MERGE` and
  `CI_MERGE_REQUEST_SOURCE_BRANCH_PROTECTED` in GitLab 16.4. (#79)
- Updated package definition to use `exports` instead of `main`.

### Miscellaneous

- Refactored predefined variable test (`test:var`) to use custom modules
  instead of `axios`, `flat` and `tabletojson`, which have moved to pure ESM.
  New module reads directly from the original markdown instead of the rendered
  HTML page. (#83)
- Updated package `stript`s to standard names. (#80)

## v8.1.0 (2023-08-22)

### Changed

- Added new variable `CI_PIPELINE_NAME` in GitLab 16.3. (#78)

## v8.0.0 (2023-06-04)

### Changed

- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (^16.13.0 || ^18.12.0 || >=20.0.0). (#77)
- BREAKING: Updated exported object to be frozen, which cleaned up types to be
  a more accurate representation of the data.

## v7.1.0 (2023-04-22)

### Changed

- Added new variables `CI_API_GRAPHQL_URL`, `CI_SERVER_SHELL_SSH_HOST`, and
  `CI_SERVER_SHELL_SSH_PORT` in GitLab 15.11. (#73, #74)
- Added previously undocumented variable `KUBECONFIG` from GitLab 14.2. (#73)

### Fixed

- Fixed README for variable `CI_COMMIT_AUTHOR`. Was previously documented
  as `ci.author`, but was corrected to `ci.config.author`. The types
  were correct.
- Updated variable tests to remove the _Deprecated_ indicator from variable
  names, which was causing tests to fail. (#75)

## v7.0.0 (2022-12-22)

### Changed

- Added new variables `CI_DEBUG_SERVICES`, `CI_JOB_TIMEOUT`, and
  `CI_PROJECT_NAMESPACE_ID` in GitLab 15.7. (#68, #69, #70)
- BREAKING: With multiple `CI_DEBUG_*` variables, a new `ci.debug` object
  was added, and the previous `ci.debugTrace` property is now available at
  `ci.debug.trace`. (#68)
- BREAKING: With multiple `CI_PROJECT_NAMESPACE_*` variables, the
  `ci.project.namespace` property was changed to an object containing all
  properties. The previous `ci.project.namespace` property was moved to
  `ci.project.namespace.fullPath`, named for consistency with the
  [GitLab API](https://docs.gitlab.com/ee/api/namespaces.html#get-namespace-by-id). (#70)

## v6.4.0 (2022-10-22)

### Changed

- Added new predefined variables in GitLab 15.5 (`CI_COMMIT_TAG_MESSAGE` and `CI_RELEASE_DESCRIPTION`) (#67)

### Miscellaneous

- Updated pipeline to have long-running jobs use GitLab shared `medium` sized runners. (#66)

## v6.3.0 (2022-09-28)

### Changed

- Added new predefined variable in GitLab 15.4 (`CI_JOB_NAME_SLUG`) (#65)

## v6.2.2 (2022-08-27)

### Fixed

- Add previously undocumented variables `CI_SERVER_TLS_CA_FILE`, `CI_SERVER_TLS_CERT_FILE`, `CI_SERVER_TLS_KEY_FILE` (see [GitLab MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/93133) for details). (#63)

### Miscellaneous

- Added CI check confirming that the defined types match the code. (#64)

## v6.2.1 (2022-08-22)

### Fixed

- Updated types to include new predefined variable in GitLab 15.3 (`CI_TEMPLATE_REGISTRY_HOST`). (#62)

## v6.2.0 (2022-08-22)

### Changed

- Added new predefined variable in GitLab 15.3 (`CI_TEMPLATE_REGISTRY_HOST`) (#59)

## v6.1.0 (2022-07-22)

### Changed

- Added new predefined variable in GitLab 15.2 (`CI_MERGE_REQUEST_TARGET_BRANCH_PROTECTED`) (#58)

### Fixed

- Remove `invert-kv` package from variables tests and replaced with limited custom implementation. (#35)
- Updated to latest dependencies

## v6.0.0 (2022-06-22)

### Changed

- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#53, #54)
- Added new predefined variable in GitLab 15.1 (`CI_PROJECT_DESCRIPTION`) (#57)

### Fixed

- Removed `CI_SERVER_TLS_CA_FILE` as runner configuration, not a predefined variable. (see [GitLab MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83972) for details). (#56)
- Updated to latest dependencies

### Miscellaneous

- Added test coverage threshold requirements (#55)

## v5.0.0 (2022-04-22)

### Changed

- BREAKING: Added TypeScript type declarations. (#44)
- Added new predefined variable in GitLab 14.10 (`CI_GITLAB_FIPS_MODE`) (#49)
- Added previously undocumented predefined variable `CI_SERVER_TLS_CA_FILE` (#50)

### Fixed

- Updated to latest dependencies, including resolving CVE-2022-0536 and CVE-2021-44906 (both dev only)

### Miscellaneous

- Added tests for Node 18
- Refactored tests per latest lint rules
- Updated `release_check` threshold to 120 days since it is primarily driven by GitLab predefined variable changes, which are flagged separately. (#48)
- Added `prettier` and disabled `eslint` formatting rules. (#51)
- Registered project for [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/5983/badge)](https://bestpractices.coreinfrastructure.org/projects/5983) badge

## v4.5.0 (2021-12-22)

### Added

- Added new predefined variables in GitLab 14.6 (`CI_JOB_JWT_V1`, `CI_JOB_JWT_V2`) (#46)

### Fixed

- Updated to latest dependencies

## v4.4.0 (2021-10-22)

### Added

- Added new predefined variable in GitLab 14.4 (`CHAT_USER_ID`) (#45)

### Fixed

- Updated to latest dependencies

## v4.3.0 (2021-09-22)

### Added

- Added new predefined variable in GitLab 14.3 (`CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX`) (#43)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Setup [renovate](https://docs.renovatebot.com/) for dependency updates (#41)

## v4.2.0 (2021-08-22)

### Added

- Added new predefined variable in GitLab 14.2 (`CI_PROJECT_CLASSIFICATION_LABEL`) (#42)

### Fixed

- Updated to latest dependencies

## v4.1.0 (2021-07-22)

### Added

- Added new predefined variable in GitLab 14.1 (`CI_MERGE_REQUEST_APPROVED`) (#39)

### Fixed

- Refactor tests for latest `eslint` rules
- Updated to latest dependencies

## v4.0.0 (2021-06-22)

### Changed

- BREAKING: Deprecated support for Node 10 and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#32)
- BREAKING: Removed deprecated predefined variables in GitLab 14.0 (`CI_PROJECT_CONFIG_PATH`) (#33)
- Added new predefined variables in GitLab 13.11 (`CI_ENVIRONMENT_ACTION`, previously undocumented) and 14.0 (`CI_ENVIRONMENT_TIER`) (#36, #37)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities
- Updated new variables test to filter out variables that GitLab has removed (#38)

### Miscellaneous

- Optimized published package to only include the minimum required files (#31)

## v3.7.0 (2021-03-26)

### Added

- Added new predefined variable in GitLab 13.10 (`CI_COMMIT_AUTHOR`), previously undocumented (#30)

### Miscellaneous

- Updated pipeline to leverage `needs` for efficiency (#29)

## v3.6.0 (2021-03-22)

### Added

- Added new predefined variables in GitLab 13.10 (`CI_JOB_STARTED_AT`, `CI_PIPELINE_CREATED_AT`) (#28)

### Fixed

- Fixed test checking for deprecated variables based on changes in GitLab source documentation (#24)
- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated pipeline to migrate from only/except to rules (#25) and to use standard NPM package collection (#27)

## v3.5.0 (2021-02-22)

### Added

- Added new predefined variables in GitLab 13.9 (`TRIGGER_PAYLOAD`) (#23)

### Fixed

- Fixed test cases for `CI_OPEN_MERGE_REQUESTS` (#22)
- Updated to latest dependencies

## v3.4.0 (2021-01-22)

### Added

- Added new predefined variables in GitLab 13.8 (`CI_PROJECT_CONFIG_PATH`) (#21)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v3.3.0 (2020-12-22)

### Added

- Added new predefined variables in GitLab 13.7 (`CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX`, `CI_DEPENDENCY_PROXY_SERVER`, `CI_DEPENDENCY_PROXY_PASSWORD`, `CI_DEPENDENCY_PROXY_USER`, `CI_OPEN_MERGE_REQUESTS`, `CI_MERGE_REQUEST_DIFF_ID`, `CI_MERGE_REQUEST_DIFF_BASE_SHA`) (#20)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#18) and GitLab Releaser (#19)
- Updated documentation for exposed module functions (#17)

## v3.2.0 (2020-10-22)

### Added

- Added new predefined variable in GitLab 13.5 (`CI_JOB_STATUS`)

### Fixed

- Updated to latest dependencies

## v3.1.0 (2020-09-22)

### Added

- Added new predefined variable in GitLab 13.4 (`CI_COMMIT_TIMESTAMP`)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v3.0.0 (2020-08-22)

### Changed

- BREAKING: Removed Node v13 compatibility since end-of-life. Compatible with all current and LTS releases (`^10.13.0 || ^12.13.0 || >=14.0.0`) (#14)

### Added

- Added new predefined variable in GitLab 13.3 (`CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY`, `CI_EXTERNAL_PULL_REQUEST_TARGET_REPOSITORY`) (#15)
- Added previously undocumented predefined variable from GitLab 13.2 (CI_DEPLOY_FREEZE) (#15)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v2.5.1 (2020-06-28)

### Fixed

- Removed GitLab variables that are not actually predefined [GitLab MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35307) (`ARTIFACT_DOWNLOAD_ATTEMPTS`, `GET_SOURCES_ATTEMPTS`, `RESTORE_CACHE_ATTEMPTS`)
- Updated to latest dependencies

## v2.5.0 (2020-06-22)

### Added

- Added new predefined variable in GitLab 13.1 (`CI_HAS_OPEN_REQUIREMENTS`) and 13.2 (`CI_PROJECT_ROOT_NAMESPACE`)

### Fixed

- Updated to latest dependencies

## v2.4.1 (2020-06-14)

### Fixed

- Remove variables that are not yet released in GitLab (`CI_MERGE_REQUEST_CHANGED_PAGE_PATHS`, `CI_MERGE_REQUEST_CHANGED_PAGE_URLS`). See https://gitlab.com/gitlab-org/gitlab/-/merge_requests/34182.
- Updated to latest dependencies

## v2.4.0 (2020-05-22)

### Changed

- Added new predefined variable in GitLab 13.0 (`CI_KUBERNETES_ACTIVE`)

### Fixed

- Updated to latest dependencies

## v2.3.0 (2020-04-22)

### Changed

- Added new predefined variable in GitLab 12.10 (`CI_JOB_JWT`)

### Fixed

- Updated to latest dependencies
- Updated to latest GitLab `license_scanning` job

## v2.2.0 (2020-03-22)

### Changed

- Added new predefined variable in GitLab 12.9 (`CI_JOB_IMAGE`, `CI_MERGE_REQUEST_CHANGED_PAGE_PATHS`, `CI_MERGE_REQUEST_CHANGED_PAGE_URLS`)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v2.1.0 (2020-02-23)

### Changed

- Added new predefined variable in GitLab 12.8 (`CI_SERVER_PORT`, `CI_SERVER_PROTOCOL`)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Split out check against GitLab predefined variables documentation as a separate CI job (#13)

## v2.0.0 (2020-01-22)

### Changed

- Added new predefined variable in GitLab 12.7 (`CI_SERVER_URL`)
- BREAKING: Removed Node v8 compatibility since end-of-life. Compatible with all current and LTS releases (`^10.13.0 || ^12.13.0 || >=13.0.0`) (#11)

### Fixed

- Updated to latest dependencies

## v1.3.1 (2019-12-29)

### Fixed

- Added predefined variable `CI_MERGE_REQUEST_EVENT_TYPE` from GitLab 12.3 that was previously undocumented (#12)
- Updated to latest dependencies

## v1.3.0 (2019-12-22)

### Changed

- Added new predefined variable in GitLab 12.6 (`CI_COMMIT_BRANCH`)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated project to use [eslint-plugin-sonarjs](https://www.npmjs.com/package/eslint-plugin-sonarjs) (#10)

## v1.2.4 (2019-11-22)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.2.3 (2019-11-17)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.2.2 (2019-11-02)

### Fixed

- Fixed error in documentation for `ci.server.host` (#8)

## v1.2.1 (2019-11-02)

### Fixed

- Fixed error in `ci.server.host`, which was returning `CI_SERVER_NAME` (#8)

### Miscellaneous

- Added tests against GitLab documentation for new or deleted predefined variables (#6)
- Updated project to use custom eslint configuration module (#7)

## v1.2.0 (2019-10-22)

### Changed

- Added new predefined variables in GitLab 12.4 (`CI_DEFAULT_BRANCH`, `CI_PROJECT_TITLE`)

## v1.1.0 (2019-10-01)

### Changed

- Added new predefined variables in GitLab 12.3

## v1.0.0 (2019-09-08)

Initial release with all variables in GitLab 12.2 (last changes in GitLab 12.1).
