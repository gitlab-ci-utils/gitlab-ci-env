declare const _exports: Readonly<{
    chat: {
        channel: string;
        input: string;
        userID: string;
    };
    ci: {
        apiGraphqlUrl: string;
        apiV4Url: string;
        buildsDir: string;
        commit: {
            author: string;
            beforeSha: string;
            branch: string;
            description: string;
            message: string;
            ref: {
                name: string;
                slug: string;
            };
            refProtected: string;
            sha: string;
            shortSha: string;
            tag: string;
            tagMessage: string;
            timestamp: string;
            title: string;
        };
        concurrentID: string;
        concurrentProjectID: string;
        configPath: string;
        debug: {
            services: string;
            trace: string;
        };
        defaultBranch: string;
        dependencyProxy: {
            directGroupImagePrefix: string;
            groupImagePrefix: string;
            password: string;
            server: string;
            user: string;
        };
        deploy: {
            freeze: string;
            password: string;
            user: string;
        };
        environment: {
            action: string;
            isDisposable: string;
            name: string;
            slug: string;
            tier: string;
            url: string;
        };
        externalPullRequest: {
            iid: string;
            source: {
                branch: {
                    name: string;
                    sha: string;
                };
                repository: string;
            };
            target: {
                branch: {
                    name: string;
                    sha: string;
                };
                repository: string;
            };
        };
        hasOpenRequirements: string;
        isCI: string;
        job: {
            groupName: string;
            id: string;
            image: string;
            manual: string;
            name: string;
            nameSlug: string;
            stage: string;
            startedAt: string;
            status: string;
            timeout: string;
            token: string;
            url: string;
        };
        kubeConfig: string;
        kubernetesActive: string;
        mergeRequest: {
            approved: string;
            assignees: string;
            description: string;
            descriptionTruncated: string;
            diff: {
                baseSha: string;
                id: string;
            };
            draft: string;
            eventType: string;
            id: string;
            iid: string;
            labels: string;
            milestones: string;
            project: {
                id: string;
                path: string;
                url: string;
            };
            refPath: string;
            source: {
                branch: {
                    name: string;
                    protected: string;
                    sha: string;
                };
                project: {
                    id: string;
                    path: string;
                    url: string;
                };
            };
            squashOnMerge: string;
            target: {
                branch: {
                    name: string;
                    protected: string;
                    sha: string;
                };
            };
            title: string;
        };
        node: {
            index: string;
            total: string;
        };
        pages: {
            domain: string;
            hostname: string;
            url: string;
        };
        pipeline: {
            createdAt: string;
            id: string;
            iid: string;
            name: string;
            schedule: {
                description: string;
            };
            source: string;
            triggered: string;
            url: string;
        };
        project: {
            classificationLabel: string;
            description: string;
            dir: string;
            id: string;
            name: string;
            namespace: {
                fullPath: string;
                id: string;
                slug: string;
            };
            openMergeRequests: string;
            path: string;
            pathSlug: string;
            repositoryLanguages: string;
            rootNamespace: string;
            title: string;
            url: string;
            visibility: string;
        };
        registry: {
            image: string;
            password: string;
            registry: string;
            url: string;
            user: string;
        };
        release: {
            description: string;
        };
        runner: {
            description: string;
            executableArch: string;
            id: string;
            revision: string;
            shortToken: string;
            tags: string;
            version: string;
        };
        server: {
            fqdn: string;
            host: string;
            isServer: string;
            name: string;
            port: string;
            protocol: string;
            revision: string;
            ssh: {
                host: string;
                port: string;
            };
            tlsCaFile: string;
            tlsCertFile: string;
            tlsKeyFile: string;
            url: string;
            version: {
                major: string;
                minor: string;
                patch: string;
                version: string;
            };
        };
        sharedEnvironment: string;
        templateRegistryHost: string;
        trigger: {
            payload: string;
            shortToken: string;
        };
    };
    gitlab: {
        features: string;
        fipsMode: string;
        isCI: string;
        user: {
            email: string;
            id: string;
            login: string;
            name: string;
        };
    };
}>;
export = _exports;
