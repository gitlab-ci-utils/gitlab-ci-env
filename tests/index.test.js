'use strict';

const setEnvironment = require('./helpers/set-environment');

describe('gitlab-ci-env results', () => {
    const testVariables = (envFile) => {
        setEnvironment(envFile);
        /* eslint-disable-next-line node/global-require -- must require module
           after env is set since it is cached with require */
        const env = require('../');
        expect(env).toMatchSnapshot();
    };

    it('should return correct results for example variables (value = name)', () => {
        expect.assertions(1);
        testVariables('names.env');
    });

    it('should return correct results for commit variables', () => {
        expect.assertions(1);
        testVariables('commit.env');
    });

    it('should return correct results for merge request variables', () => {
        expect.assertions(1);
        testVariables('mergeRequest.env');
    });

    it('should return correct results for schedules variables', () => {
        expect.assertions(1);
        testVariables('schedules.env');
    });

    it('should return correct results for tag variables', () => {
        expect.assertions(1);
        testVariables('tag.env');
    });
});
