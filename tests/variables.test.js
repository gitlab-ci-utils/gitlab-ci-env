'use strict';

const { flatten } = require('./helpers/flatten');
const {
    getGitLabPredefinedVariableData
} = require('./helpers/gitlab-variables');
const setEnvironment = require('./helpers/set-environment');

describe('gitlab predefined variables', () => {
    const envNamesFile = 'names.env';
    const variableThreshold = 10;

    const invertKeyValue = (object) => {
        const invertedObject = {};
        for (const [key, value] of Object.entries(object)) {
            invertedObject[value] = key;
        }
        return invertedObject;
    };

    const getCurrentVariable = () => {
        // Set env var to variable names
        setEnvironment(envNamesFile);
        /* eslint-disable-next-line node/global-require -- must be required after
           environment variables are set */
        const env = require('../index');
        // Flatten return object and invert key/value to allow lookup by GitLab var name
        return invertKeyValue(flatten(env));
    };

    let currentVariable, deletedGitLabVariable, gitLabPredefinedVariable;
    const newGitLabVariable = [];

    beforeAll(async () => {
        // Get GitLab predefined variables and current object variables
        gitLabPredefinedVariable = await getGitLabPredefinedVariableData();
        currentVariable = getCurrentVariable();

        // Copy current variables, will remove any found from GitLab
        // so those remaining have been deleted
        deletedGitLabVariable = { ...currentVariable };

        // Iterate through latest GitLab predefined var to find any
        // new (newGitLabVar) or removed (deletedGitLabVar) variables.
        for (const variable of gitLabPredefinedVariable) {
            if (currentVariable[variable.variable]) {
                delete deletedGitLabVariable[variable.variable];
            } else {
                newGitLabVariable.push(variable);
            }
        }
    });

    it('should return GitLab predefined variables from documentation', () => {
        expect.assertions(2);
        // Check that GitLab predefined variables were properly returned, i.e. that an
        // appropriate number of value were returned (within a predefined threshold)
        const minVariable =
            Object.entries(currentVariable).length - variableThreshold;
        const maxVariable =
            Object.entries(currentVariable).length + variableThreshold;
        expect(gitLabPredefinedVariable.length).toBeGreaterThan(minVariable);
        expect(gitLabPredefinedVariable.length).toBeLessThan(maxVariable);
    });

    it('should not have new variables', () => {
        expect.assertions(1);
        expect(newGitLabVariable).toHaveLength(0);
    });

    it('should not have variables that have been removed', () => {
        expect.assertions(1);
        expect(Object.entries(deletedGitLabVariable)).toHaveLength(0);
    });
});
