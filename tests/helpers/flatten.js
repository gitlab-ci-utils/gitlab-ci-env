'use strict';
//
// This is a simplified version of the flat package, available at
// https://github.com/hughsk/flat/.
//
// Copyright (c) 2014, Hugh Kennedy
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// 3. Neither the name of the  nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

/**
 * Function to flatten an object, e.g. { a: { b: 1 } } becomes { 'a.b': 1 }.
 *
 * @module flatten
 */

/**
 * Flattens the current object.
 *
 * @param   {object} target The object to flatten.
 * @returns {object}        The flattened object.
 * @static
 * @public
 */
const flatten = (target) => {
    const output = {};

    // eslint-disable-next-line complexity -- test helper function
    const step = (object, previous) => {
        for (const [key, value] of Object.entries(object)) {
            const isObject = typeof value === 'object' && value !== null;

            const newKey = previous ? `${previous}.${key}` : key;

            if (isObject && Object.keys(value).length > 0) {
                step(value, newKey);
                // eslint-disable-next-line no-continue -- modified package
                continue;
            }

            output[newKey] = value;
        }
    };

    step(target);

    return output;
};

module.exports.flatten = flatten;
