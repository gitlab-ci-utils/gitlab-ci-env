'use strict';

/**
 * Functions to extract GitLab predefined variable information from
 * documentation.
 *
 * @module gitlab-variables
 */

const gitLabPredefinedVariableUrl =
    'https://gitlab.com/gitlab-org/gitlab/-/raw/master/doc/ci/variables/predefined_variables.md';
const deprecatedText = ' (Deprecated)';

/**
 * Formats variable name to remove any backticks.
 *
 * @param   {string} variable The variable name.
 * @returns {string}          The formatted variable name.
 * @static
 * @private
 */
const formatVariable = (variable) => variable.replaceAll('`', '');

/**
 * Determines if a markdown table row is a variable. Filter out any header
 * or separator rows.
 *
 * @param   {string[]} cells An array of cells from a markdown table row.
 * @returns {boolean}        True if the row is a variable, false otherwise.
 * @static
 * @private
 */
const isVariable = (cells) => {
    let variable = true;

    // If all cells are dashes, then this is a separator row
    if (cells.every((cell) => cell.replaceAll('-', '').length === 0)) {
        variable = false;
    }

    // If first cell is "Variable", then this is the header row
    if (cells[0] === 'Variable') {
        variable = false;
    }

    // If the first cell contains deprecatedText, then the variable is
    // deprecated and should be excluded.
    if (cells[0].includes(deprecatedText)) {
        variable = false;
    }

    return variable;
};

/**
 * Object representing the properties of a GitLab predefined variable.
 *
 * @typedef  {object} Variable
 * @property {string} [availability] The availability of the variable.
 * @property {string} description    The variable description.
 * @property {string} variable       The variable name.
 */

// eslint-disable-next-line jsdoc/require-returns-check -- implicitly returns undefined
/**
 * Parses a line of markdown table cells to extract variable information.
 *
 * @param   {string}   line The line of markdown table cells to parse.
 * @returns {Variable}      An object containing the variable data  is a variable.
 * @throws  {Error}         If the number of cells is unexpected.
 * @static
 * @private
 */
// eslint-disable-next-line consistent-return -- implicitly returns undefined
const getVariableFromLine = (line) => {
    const cells = line
        .split('|')
        .map((cell) => cell.trim())
        // Filters out empty first and last cell from split
        .filter((cell) => cell !== '');

    // Main table has 3 columns, MR/PR tables only have 2 columns.
    const mainTableColumns = 3;
    const mrTableColumns = 2;
    if (isVariable(cells)) {
        let availability, description, variable;
        if (cells.length === mainTableColumns) {
            [variable, availability, description] = cells;
        } else if (cells.length === mrTableColumns) {
            [variable, description] = cells;
        } else {
            throw new Error(`Unexpected number of cells: ${cells.length}`);
        }
        return {
            availability,
            description,
            variable: formatVariable(variable)
        };
    }
};

/**
 * Gets GitLab predefined variable data from th GitLab documentation.
 *
 * @returns {Variable[]} An array of objects containing GitLab predefined variable data.
 * @async
 * @static
 * @public
 */
const getGitLabPredefinedVariableData = async () => {
    /* eslint-disable-next-line node/no-unsupported-features/node-builtins --
       allow for tests */
    const response = await fetch(gitLabPredefinedVariableUrl);

    // Extract table data from original markdown
    const markdownText = await response.text();
    const lines = markdownText
        .split('\n')
        .map((line) => line.trim())
        .filter((line) => line.startsWith('|'));

    const variables = [];

    // Get only values that are table body rows
    for (const line of lines) {
        const variable = getVariableFromLine(line);
        if (variable) {
            variables.push(variable);
        }
    }

    // Filter out any variable whose Description starts with "Removed"
    return variables.filter(
        (variable) => !variable.description.startsWith('Removed')
    );
};

module.exports = { getGitLabPredefinedVariableData };
