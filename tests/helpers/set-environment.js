'use strict';

/**
 * Functions for managing the current environment variables.
 *
 * @module set-environment
 */

const fs = require('node:fs');
const dotenv = require('dotenv');

/**
 * Configures the current environment with the given .env file variables.
 *
 * @param {string} envFile The .env file to apply to environment.
 * @static
 * @public
 */
const setEnvironment = (envFile) => {
    // Clear module cache so env can be reset with new file
    // eslint-disable-next-line no-undef -- Test helper function, global value.
    jest.resetModules();
    // Apply new env from the given file
    const envConfig = dotenv.parse(
        fs.readFileSync(`./tests/fixtures/${envFile}`)
    );
    // eslint-disable-next-line guard-for-in -- object generated from dotenv
    for (const k in envConfig) {
        process.env[k] = envConfig[k];
    }
};

module.exports = setEnvironment;
